//
//  Chaine.h
//  ListeChainee
//
//  Created by Tatadmound on 13/04/2021.
//

#ifndef Chaine_h
#define Chaine_h
typedef struct Chaine Chaine;
struct Chaine {
    ChaineElement *premier;
    int chaineLength;
};

#endif /* Chaine_h */
