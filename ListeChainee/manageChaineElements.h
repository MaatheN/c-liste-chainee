//
//  manageChaineElements.h
//  ListeChainee
//
//  Created by Tatadmound on 13/04/2021.
//

#ifndef manageChaineElements_h
#define manageChaineElements_h

#include <stdio.h>
#include <stdlib.h>
#include "ChaineElement.h"
#include "Chaine.h"

//return 0 -> all right, 1-> Error
//initialiser la liste
Chaine* initChaine(void);
//ajouter le premier élément de la liste
char addElement(Chaine *chainePointer, int value);
//au milieu
char addElementNextTo(Chaine *chainePointer, ChaineElement *chaineElementTarget, int value);
//supprimer le premier élément de la liste
char deleteElement(Chaine *chainePointer);
//delete a specifique targetElement
char deleteThisElement(Chaine *chainePointer, ChaineElement *targetElement);
//afficher le contenu de la liste
char showChaine(Chaine *chainePointer);
//supprimer la liste entière
char deleteChaine(Chaine *chainePointer);
//return the length of the chaine as an int
int getChaineLength(Chaine *chainePointer);
#endif /* manageChaineElements_h */
