//
//  ChaineElement.h
//  ListeChainee
//
//  Created by Tatadmound on 13/04/2021.
//

#ifndef ChaineElement_h
#define ChaineElement_h
typedef struct ChaineElement ChaineElement;
struct ChaineElement {
    int value;
    ChaineElement *nextValue;
};

#endif /* ChaineElement_h */
