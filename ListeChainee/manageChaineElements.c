//
//  manageChaineElements.c
//  ListeChainee
//
//  Created by Tatadmound on 13/04/2021.
//

#include "manageChaineElements.h"

Chaine* initChaine(void){
    Chaine *chainePointer = malloc(sizeof(*chainePointer));
    ChaineElement *chaineElementPointer = malloc(sizeof(*chaineElementPointer));
    
    if (chainePointer == NULL || chaineElementPointer == NULL) {
        exit(EXIT_FAILURE);
    }
    
    chaineElementPointer->nextValue=NULL;
    chaineElementPointer->value=0;
    chainePointer->premier=chaineElementPointer;
    chainePointer->chaineLength=1;
    
    return chainePointer;
}
char addElement(Chaine *chainePointer, int value){
    //new ChaineElement
    ChaineElement *newChaineElement = malloc(sizeof(ChaineElement));
    //try error
    if (chainePointer == NULL || newChaineElement == NULL) {
        printf("\nErreur lors de l'ajout d'un nouvel element de chaine\n");
    }
    //assign value
    newChaineElement->value = value;
    //assign adresse of old first
    newChaineElement->nextValue = chainePointer->premier;
    //assign this pointer to first element in Chaine
    chainePointer->premier = newChaineElement;
    chainePointer->chaineLength++;
    return 0;
}
char addElementNextTo(Chaine *chainePointer, ChaineElement *chaineElementTarget, int value){
    //create an element with the user's value
    ChaineElement *newChaineElement = malloc(sizeof(ChaineElement));
    //set newElement values
    newChaineElement->nextValue = chaineElementTarget->nextValue;
    newChaineElement->value = value;
    //replace chaineElementTarget.next by our new Element
    chaineElementTarget->nextValue = newChaineElement;
    chainePointer->chaineLength++;
    return 0;
}
char deleteElement(Chaine *chainePointer){
    //check if Chaine exist
    if (chainePointer==NULL) {
        printf("\nErreur lors de la suppression de l'élement\n");
        exit(EXIT_FAILURE);
    }
    if (chainePointer->premier!=NULL) {
        //get the element to delete
        ChaineElement *elementToDelete = chainePointer->premier;
        //turn second element to first one
        chainePointer->premier = chainePointer->premier->nextValue;
        //delete the element to delete
        free(elementToDelete);
    }
    chainePointer->chaineLength--;
    return 0;
}
char deleteThisElement(Chaine *chainePointer, ChaineElement *targetElement){
    if(chainePointer == NULL){
        printf("\nErreur lors de la suppression, la chaine semble vide.\n");
        exit(EXIT_FAILURE);
    }
    //find the previous element of the target in the list
    ChaineElement *previousTargetElementPointer = chainePointer->premier;
    while (previousTargetElementPointer->nextValue != targetElement) {
        if (previousTargetElementPointer == NULL) {
            printf("\nErreur, impossible de retrouver l'élément précédent la cible à supprimmer.\n");
            exit(EXIT_FAILURE);
        }
        previousTargetElementPointer = previousTargetElementPointer->nextValue;
    }
    //get the nextElement of the targetElement and set it to the previous.next
    previousTargetElementPointer->nextValue = targetElement->nextValue;
    //free the element
    free(targetElement);
    chainePointer->chaineLength--;
    return 0;
}
char showChaine(Chaine *chainePointer){
    if (chainePointer == NULL) {
        exit(EXIT_FAILURE);
    }
    //1 from chaine , get the first chaineElement's pointer
    ChaineElement *currentChaineElementPointer = chainePointer->premier;
    //3 read the next pointer value, if its not null goto 2
    while (currentChaineElementPointer != NULL) {
        printf("\n%d", currentChaineElementPointer->value);
        currentChaineElementPointer=currentChaineElementPointer->nextValue;
    }
    //4 say 'fin de la chaine'
    printf("\nFin de la chaine\nTaille de celle-ci : %d\n", chainePointer->chaineLength);
    return 0;
}
char deleteChaine(Chaine *chainePointer){
    //2) if the first element of the list is not null 1)
    while (chainePointer->premier != NULL) {
    //1) remove the first element of the list with deleteElement
        deleteElement(chainePointer);
    }
    //3) say 'chaine deleted with success'
    printf("\nChaine supprimée avec succès\n");
    return 0;
}
int getChaineLength(Chaine *chainePointer){
    return chainePointer->chaineLength;
}
