//
//  main.c
//  ListeChainee
//
//  Created by Tatadmound on 13/04/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include "manageChaineElements.h"

int main(int argc, const char * argv[]) {
    Chaine *currentChainePointer = initChaine();
    
    addElement(currentChainePointer, 4);
    addElement(currentChainePointer, 8);
    addElement(currentChainePointer, 15);
    addElement(currentChainePointer, 1);
    addElement(currentChainePointer, 9);
    addElement(currentChainePointer, 18);
    
    showChaine(currentChainePointer);
    
    //3rd element
    ChaineElement *targetElementPointer = currentChainePointer->premier->nextValue->nextValue;
    addElementNextTo(currentChainePointer, targetElementPointer, 22);
    showChaine(currentChainePointer);
    
    //delete 3rd element
    deleteThisElement(currentChainePointer, targetElementPointer);
    showChaine(currentChainePointer);
    
    printf("\nVoici le nombre d'éléments dans cette liste : %d\n", getChaineLength(currentChainePointer));
    return 0;
}
